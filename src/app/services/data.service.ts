import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Storage } from '@ionic/storage';
import { Componente, RespuestaCiudades } from '../interfaces/interfaces';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor( private http: HttpClient,
               private storage: Storage ) { }

  getMenuOpts() {
    return this.http.get<Componente>('/assets/data/menu.json');
  }
  guardarCiudades(ciudades: RespuestaCiudades[]) {
    this.storage.set('ciudades', ciudades);
  }
  async cargarCiudades() {
    const c = await this.storage.get('ciudades');
    return c;
  }
}
