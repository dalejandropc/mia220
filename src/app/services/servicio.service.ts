import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { RespuestaPatologias, RespuestaSintomas, RespuestaNucleo, RespuestaUsuario, RespuestaCiudades } from '../interfaces/interfaces';
import { Md5 } from 'ts-md5/dist/md5';
import { environment } from '../../environments/environment';

const path = environment.path;
const movil = environment.movil;
const apiKey = environment.apiKey;
const esquema = environment.esquema;

@Injectable({
  providedIn: 'root'
})
export class ServicioService {

  key = Md5.hashStr( movil + apiKey + esquema);
  constructor(private http: HttpClient ) { }

  private ejecutarQueryGet<T>( query: string ) {
    query = path + query + `key=${this.key}&movil=${movil}&esquema=${esquema}`;
    return this.http.get( query  );
  }
  getUsuario(imei: string) {
    return this.ejecutarQueryGet<RespuestaUsuario>(`ingresoSrv?imei=${imei}&`);
  }
  getCiudades() {
    return this.ejecutarQueryGet<RespuestaCiudades>(`ciudadesSrv?`);
  }
  getPatologias() {
    return this.ejecutarQueryGet<RespuestaPatologias>(`cargarPatologiasSrv?`);
  }
  getSintomas(pat: number) {
    return this.ejecutarQueryGet<RespuestaSintomas>(`cargarSintomasSrv?dolencia=${pat}&`);
  }
  getNucleo( nucleo: number ) {
    return this.ejecutarQueryGet<RespuestaNucleo>(`cargarNucleoSrv?id=${nucleo}&`);
  }
  guardarPersona( data: string ) {
    return this.ejecutarQueryGet<RespuestaNucleo>(`cargarNucleoSrv?data=${data}&`);
  }

}
