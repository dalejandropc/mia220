
export interface RespuestaUsuario {
    id_per?: string;
    fk_ciu?: number;
    movil?: string;
    nombres: string;
    apellidos: string;
    alias?: string;
    telefono: string;
    direccion: string;
    direccion2?: string;
    email?: string;
    fecha_nacimiento?: Date;
    tipo_sangre?: string;
    rh?: string;
    sisben?: number;
    eps?: string;
}

export interface RespuestaCiudades {
    departamento: string;
    divipola: string;
    ciudades: string;
}

export interface RespuestaPatologias {
    id_dol: number;
    dolencia: string;
    observacion?: string;
    imagen: string;
    Error?: number;
}
export interface RespuestaSintomas {
    id_sin: number;
    sintoma: string;
    requiere_obs: string;
    observacion?: string;
    Error?: number;
}

export interface RespuestaNucleo {
    id_per?: number;
    fk_ciu?: number;
    movil?: string;
    nombres?: string;
    apellidos?: string;
    alias?: string;
    telefono?: string;
    direccion?: string;
    direccion2?: string;
    email?: string;
    fecha_nacimiento?: string;
    tipo_sangre?: string;
    rh?: string;
    sisben?: string;
    eps?: string;
    localizacion?: string;
    parentesco?: string;
    Error?: number;
}
export interface Componente {
    icon: string;
    name: string;
    redirectTo: string;
}
