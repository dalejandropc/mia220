import { Component, OnInit } from '@angular/core';
import { NavParams } from '@ionic/angular';
import { RespuestaPatologias, RespuestaSintomas } from '../../interfaces/interfaces';
import { ActivatedRoute } from '@angular/router';
import { ServicioService } from '../../services/servicio.service';

@Component({
  selector: 'app-sintomas',
  templateUrl: './sintomas.page.html',
  styleUrls: ['./sintomas.page.scss'],
})
export class SintomasPage implements OnInit {

  sintomas: RespuestaSintomas[] = [];

  private patologia: RespuestaPatologias = null;

  constructor(private sintomasService: ServicioService, private route: ActivatedRoute) {
  }

  ngOnInit() {
    const p: string = this.route.snapshot.paramMap.get('patologia');
    this.patologia = JSON.parse(p);
    if ( this.patologia ) {
      this.sintomasService.getSintomas( this.patologia.id_dol )
          .subscribe( (resp: RespuestaSintomas) => {
            this.sintomas.push( resp );
          });
      console.log('Sintomas:', this.sintomas);
    }
  }

}
