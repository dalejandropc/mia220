import { Component, ViewChild } from '@angular/core';
import { ServicioService } from '../../services/servicio.service';
import { RespuestaNucleo } from '../../interfaces/interfaces';
import { IonList, NavController } from '@ionic/angular';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {

  @ViewChild('listaNucleo', { read: false, static: false } ) lista: IonList;

  nucleo: any[] = [];
  private idPersona = environment.usuario.id_per;
  constructor( private nucleoService: ServicioService, public navCtrl: NavController ) {}

  ngOnInit(): void {
    this.nucleoService.getNucleo( this.idPersona )
      .subscribe( resp => {
        this.nucleo.push( resp );
      });
  }
  aSintomas( persona: RespuestaNucleo ) {
    this.lista.closeSlidingItems();
  }
  aModificar( persona: RespuestaNucleo ) {
    this.lista.closeSlidingItems();
  }
  onClick() {
    this.navCtrl.navigateRoot('/nueva-persona');
  }

}
