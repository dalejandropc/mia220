import { Component, OnInit } from '@angular/core';
import { ServicioService } from '../../services/servicio.service';
import { RespuestaPatologias } from '../../interfaces/interfaces';
import { NavController } from '@ionic/angular';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page implements OnInit {

  patologias: RespuestaPatologias[] = [];

  constructor(private patologiaService: ServicioService,
              private usuarioService: ServicioService,
              public navCtrl: NavController ) {

  }
  ngOnInit() {
    this.login();
  }

  aSintomas(patologia: RespuestaPatologias) {
    if ( this.patologias ) {
      const myObjStr = JSON.stringify(patologia);
      this.navCtrl.navigateRoot('/sintomas/' + myObjStr);
    }
  }

  login() {
    if ( !environment.usuario ) {
      const imei = environment.imei;
      this.usuarioService.getUsuario(imei)
        .subscribe( resp => {
          console.log( 'respuesta',  resp );
          if ( resp ) {
            environment.usuario = resp;
            this.cargarPatologias();
          } else {
            this.navCtrl.navigateRoot('/tabs/tab3');
          }
      });
    }
  }
  cargarPatologias() {
    if ( environment.usuario.id_per ) {
      this.patologiaService.getPatologias()
        .subscribe( ( resp: RespuestaPatologias ) => {
          this.patologias.push( resp );
          if ( resp[0].Error ) {
            this.navCtrl.navigateRoot('/tabs/tab3');
          }
      });
    } else {
      this.navCtrl.navigateRoot('/nueva-persona');
    }
  }
}
