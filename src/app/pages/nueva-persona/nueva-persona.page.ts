import { Component, OnInit } from '@angular/core';
import { environment } from '../../../environments/environment';
import { DataService } from '../../services/data.service';
import { RespuestaCiudades, RespuestaUsuario } from '../../interfaces/interfaces';
import { ServicioService } from '../../services/servicio.service';
import { FormBuilder } from '@angular/forms';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-nueva-persona',
  templateUrl: './nueva-persona.page.html',
  styleUrls: ['./nueva-persona.page.scss'],
})


export class NuevaPersonaPage implements OnInit {

  public idPersona = environment.usuario.id_per;
  public nombres = '';
  public apellidos = '';
  public movil = '';
  public telefono = '';
  public email = '';
  public direccion = '';
  public fkCiu = '';
  public direccion2 = '';
  public rh = '';
  public tipoSangre = '';
  public eps = '';
  public parentesco = '';
  public genero = '';
  public fkDpto = '';
  public localizacion = '';
  public fechaNacimiento = null;
  departamentos: RespuestaCiudades[] = [];

  constructor( public navCtrl: NavController,
               private ciudadDbService: ServicioService,
               private ciudadesLSService: DataService ) {
              }

  ngOnInit() {
    this.cargarDepartamentosLC();
  }
  guardarUsuario() {
    let cad = `fk_ciu=${this.fkCiu}, movil=${this.movil}, nombres=${this.nombres}, apellidos=${this.apellidos},`;
    cad += ` telefono=${this.telefono}, direccion=${this.direccion}, `;
    cad += `email=${this.email}, fecha_nacimiento=${this.fechaNacimiento}, tipo_sangre=${this.tipoSangre}, rh=${this.rh}, `;
    cad += `eps=${this.eps}, localizacion=${this.localizacion}, fk_dpto=${this.fkDpto}`;
    console.log(cad);
  }
  async cargarDepartamentosLC() {
    const c = await this.ciudadesLSService.cargarCiudades();
    if ( c ) {
      this.departamentos = c;
    }
    if ( this.departamentos.length === 0 ) {
      this.cargarCiudadesDptosDB();
    }
  }
  cargarCiudadesDptosDB() {
    this.ciudadDbService.getCiudades()
      .subscribe( (resp: RespuestaCiudades[]) => {
        if (resp) {
          this.departamentos = resp;
          this.ciudadesLSService.guardarCiudades( this.departamentos );
        }
    });
  }
  cargarCiudades( event ) {
    alert();
    console.log('iDpto', event );
  }
}
