import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { IonicModule } from '@ionic/angular';

import { NuevaPersonaPage } from './nueva-persona.page';
import { ComponentsModule } from '../../components/components.module';

import { NuevaPersonaPageRoutingModule } from './nueva-persona-routing.module';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    IonicModule,
    NuevaPersonaPageRoutingModule,
    ComponentsModule,
    FormsModule
  ],
  declarations: [NuevaPersonaPage]
})
export class NuevaPersonaPageModule {}
