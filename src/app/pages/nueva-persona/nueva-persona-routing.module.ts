import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NuevaPersonaPage } from './nueva-persona.page';

const routes: Routes = [
  {
    path: '',
    component: NuevaPersonaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NuevaPersonaPageRoutingModule {}
