import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./pages/tabs/tabs.module').then(m => m.TabsPageModule)
  },
  {
    path: 'nueva-persona',
    loadChildren: () => import('./pages/nueva-persona/nueva-persona.module').then( m => m.NuevaPersonaPageModule)
  },
  {
    path: 'sintomas/:patologia',
    loadChildren: () => import('./pages/sintomas/sintomas.module').then( m => m.SintomasPageModule)
  }
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
