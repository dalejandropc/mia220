PGDMP                         x            pandemia    12.2    12.2 f    �           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                      false            �           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                      false            �           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                      false            �           1262    242746    pandemia    DATABASE     �   CREATE DATABASE pandemia WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'Spanish_Spain.1252' LC_CTYPE = 'Spanish_Spain.1252';
    DROP DATABASE pandemia;
                postgres    false                        2615    242747    col    SCHEMA        CREATE SCHEMA col;
    DROP SCHEMA col;
                postgres    false            �            1259    242790    departamento    TABLE     �   CREATE TABLE col.departamento (
    id_dpto smallint NOT NULL,
    departamento character varying(80) NOT NULL,
    divipola character varying(3),
    iso character varying(3),
    ciudades jsonb
);
    DROP TABLE col.departamento;
       col         heap    postgres    false    7            �           0    0    TABLE departamento    COMMENT     N   COMMENT ON TABLE col.departamento IS 'Departamentos o estados de la nación';
          col          postgres    false    210            �            1259    242788    departamento_id_dpto_seq    SEQUENCE     �   CREATE SEQUENCE col.departamento_id_dpto_seq
    AS smallint
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ,   DROP SEQUENCE col.departamento_id_dpto_seq;
       col          postgres    false    210    7            �           0    0    departamento_id_dpto_seq    SEQUENCE OWNED BY     O   ALTER SEQUENCE col.departamento_id_dpto_seq OWNED BY col.departamento.id_dpto;
          col          postgres    false    209            �            1259    242779    dolencia    TABLE     �   CREATE TABLE col.dolencia (
    id_dol bigint NOT NULL,
    dolencia character varying(80) NOT NULL,
    observacion character varying(500),
    imagen character varying(150)
);
    DROP TABLE col.dolencia;
       col         heap    postgres    false    7            �           0    0    TABLE dolencia    COMMENT     i   COMMENT ON TABLE col.dolencia IS 'Diferentes tipos de enfermedad que se pueden manejar en la aplición';
          col          postgres    false    208            �            1259    242777    dolencia_id_dol_seq    SEQUENCE     y   CREATE SEQUENCE col.dolencia_id_dol_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 '   DROP SEQUENCE col.dolencia_id_dol_seq;
       col          postgres    false    7    208            �           0    0    dolencia_id_dol_seq    SEQUENCE OWNED BY     E   ALTER SEQUENCE col.dolencia_id_dol_seq OWNED BY col.dolencia.id_dol;
          col          postgres    false    207            �            1259    242767    funcionario    TABLE       CREATE TABLE col.funcionario (
    id_fun bigint NOT NULL,
    fk_per bigint NOT NULL,
    fk_pla integer,
    usuario character varying(32) NOT NULL,
    password character varying,
    esquema character varying(10) DEFAULT 'general_col'::character varying NOT NULL
);
    DROP TABLE col.funcionario;
       col         heap    postgres    false    7            �           0    0    TABLE funcionario    COMMENT     ~   COMMENT ON TABLE col.funcionario IS 'Esta tabla guarda los funcionarios que tienen accesos a la plataforma, son personas...';
          col          postgres    false    206            �            1259    242765    funcionario_id_fun_seq    SEQUENCE     |   CREATE SEQUENCE col.funcionario_id_fun_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 *   DROP SEQUENCE col.funcionario_id_fun_seq;
       col          postgres    false    206    7            �           0    0    funcionario_id_fun_seq    SEQUENCE OWNED BY     K   ALTER SEQUENCE col.funcionario_id_fun_seq OWNED BY col.funcionario.id_fun;
          col          postgres    false    205            �            1259    250969    mercado    TABLE       CREATE TABLE col.mercado (
    id_mer integer NOT NULL,
    fk_per bigint NOT NULL,
    estado character varying DEFAULT 'PENDIENTE'::character varying NOT NULL,
    observacion character varying,
    creacion timestamp with time zone DEFAULT now() NOT NULL
);
    DROP TABLE col.mercado;
       col         heap    postgres    false    7            �           0    0    TABLE mercado    COMMENT     \   COMMENT ON TABLE col.mercado IS 'Tabla para guardar las solicitudes de mercados a regalar';
          col          postgres    false    226            �            1259    250967    mercado_id_mer_seq    SEQUENCE     �   CREATE SEQUENCE col.mercado_id_mer_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 &   DROP SEQUENCE col.mercado_id_mer_seq;
       col          postgres    false    7    226            �           0    0    mercado_id_mer_seq    SEQUENCE OWNED BY     C   ALTER SEQUENCE col.mercado_id_mer_seq OWNED BY col.mercado.id_mer;
          col          postgres    false    225            �            1259    242903    persona_nucleo    TABLE     �   CREATE TABLE col.persona_nucleo (
    id_nuc bigint NOT NULL,
    fk_cabeza bigint NOT NULL,
    fk_integrante bigint NOT NULL,
    parentesco character varying NOT NULL
);
    DROP TABLE col.persona_nucleo;
       col         heap    postgres    false    7            �           0    0    TABLE persona_nucleo    COMMENT     �   COMMENT ON TABLE col.persona_nucleo IS 'Guarda las relaciones de cabeza de familia con los demas integrantes de su nucleo familiar';
          col          postgres    false    220            �            1259    242901    nucleo_id_nuc_seq    SEQUENCE     w   CREATE SEQUENCE col.nucleo_id_nuc_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 %   DROP SEQUENCE col.nucleo_id_nuc_seq;
       col          postgres    false    7    220            �           0    0    nucleo_id_nuc_seq    SEQUENCE OWNED BY     I   ALTER SEQUENCE col.nucleo_id_nuc_seq OWNED BY col.persona_nucleo.id_nuc;
          col          postgres    false    219            �            1259    242828    persona    TABLE     3  CREATE TABLE col.persona (
    id_per bigint NOT NULL,
    fk_ciu integer,
    movil character varying(20) NOT NULL,
    nombres character varying(150) NOT NULL,
    apellidos character varying(150),
    alias character varying(40),
    telefono character varying(40),
    direccion character varying(150),
    direccion2 character varying(150),
    email character varying(120),
    fecha_nacimiento date,
    tipo_sangre character(1),
    rh character(1),
    sisben integer,
    eps character varying,
    localizacion jsonb,
    imei character varying(15)
);
    DROP TABLE col.persona;
       col         heap    postgres    false    7            �           0    0    TABLE persona    COMMENT     h   COMMENT ON TABLE col.persona IS 'Tabla temporal para guardar una persona antes de confirmar su móvil';
          col          postgres    false    214            �            1259    242826    persona_id_per_seq    SEQUENCE     x   CREATE SEQUENCE col.persona_id_per_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 &   DROP SEQUENCE col.persona_id_per_seq;
       col          postgres    false    214    7            �           0    0    persona_id_per_seq    SEQUENCE OWNED BY     C   ALTER SEQUENCE col.persona_id_per_seq OWNED BY col.persona.id_per;
          col          postgres    false    213            �            1259    242839    persona_sintoma    TABLE     �   CREATE TABLE col.persona_sintoma (
    id_ps bigint NOT NULL,
    fk_per bigint NOT NULL,
    fk_sin bigint NOT NULL,
    valor boolean NOT NULL,
    observacion jsonb,
    creacion timestamp with time zone DEFAULT now() NOT NULL
);
     DROP TABLE col.persona_sintoma;
       col         heap    postgres    false    7            �           0    0    TABLE persona_sintoma    COMMENT     Y   COMMENT ON TABLE col.persona_sintoma IS 'Sintomas que tiene una persona para evaluarlo';
          col          postgres    false    216            �            1259    250941    persona_sintoma_estado    TABLE     �   CREATE TABLE col.persona_sintoma_estado (
    id_pse bigint NOT NULL,
    fk_ps bigint NOT NULL,
    fk_fun bigint,
    fk_est integer NOT NULL,
    observacion character varying(1500),
    creacion timestamp with time zone DEFAULT now() NOT NULL
);
 '   DROP TABLE col.persona_sintoma_estado;
       col         heap    postgres    false    7            �           0    0    TABLE persona_sintoma_estado    COMMENT     k   COMMENT ON TABLE col.persona_sintoma_estado IS 'Guarda los estados del cargue de sintomas de una persona';
          col          postgres    false    222            �            1259    250939 !   persona_sintoma_estado_id_pse_seq    SEQUENCE     �   CREATE SEQUENCE col.persona_sintoma_estado_id_pse_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 5   DROP SEQUENCE col.persona_sintoma_estado_id_pse_seq;
       col          postgres    false    7    222            �           0    0 !   persona_sintoma_estado_id_pse_seq    SEQUENCE OWNED BY     a   ALTER SEQUENCE col.persona_sintoma_estado_id_pse_seq OWNED BY col.persona_sintoma_estado.id_pse;
          col          postgres    false    221            �            1259    242837    persona_sintoma_id_ps_seq    SEQUENCE        CREATE SEQUENCE col.persona_sintoma_id_ps_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE col.persona_sintoma_id_ps_seq;
       col          postgres    false    216    7            �           0    0    persona_sintoma_id_ps_seq    SEQUENCE OWNED BY     Q   ALTER SEQUENCE col.persona_sintoma_id_ps_seq OWNED BY col.persona_sintoma.id_ps;
          col          postgres    false    215            �            1259    242748 
   plataforma    TABLE     �   CREATE TABLE col.plataforma (
    id_pla integer NOT NULL,
    fk_ciu integer NOT NULL,
    fk_dpt smallint,
    plataforma character varying(50) NOT NULL
);
    DROP TABLE col.plataforma;
       col         heap    postgres    false    7            �            1259    242751    plataforma_id_pla_seq    SEQUENCE     �   CREATE SEQUENCE col.plataforma_id_pla_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 )   DROP SEQUENCE col.plataforma_id_pla_seq;
       col          postgres    false    203    7            �           0    0    plataforma_id_pla_seq    SEQUENCE OWNED BY     I   ALTER SEQUENCE col.plataforma_id_pla_seq OWNED BY col.plataforma.id_pla;
          col          postgres    false    204            �            1259    242816    sintoma    TABLE     �   CREATE TABLE col.sintoma (
    id_sin bigint NOT NULL,
    fk_dol bigint NOT NULL,
    sintoma character varying(150) NOT NULL,
    requiere_obs boolean DEFAULT false NOT NULL,
    observacion character varying(1000)
);
    DROP TABLE col.sintoma;
       col         heap    postgres    false    7            �           0    0    TABLE sintoma    COMMENT     T   COMMENT ON TABLE col.sintoma IS 'Listado de sintomas que puede tener una dolencia';
          col          postgres    false    212            �           0    0    COLUMN sintoma.requiere_obs    COMMENT     �   COMMENT ON COLUMN col.sintoma.requiere_obs IS 'Indica si se va a mostrar un cuadro de dialogo al usuario para que llene data adicional';
          col          postgres    false    212            �           0    0    COLUMN sintoma.observacion    COMMENT     �   COMMENT ON COLUMN col.sintoma.observacion IS 'Si requiere data adicional de parte del usuario, la pregunta u observación que se le va a hacer';
          col          postgres    false    212            �            1259    250953    sintoma_estado    TABLE     b   CREATE TABLE col.sintoma_estado (
    id_se integer NOT NULL,
    estado character varying(40)
);
    DROP TABLE col.sintoma_estado;
       col         heap    postgres    false    7            �           0    0    TABLE sintoma_estado    COMMENT     �   COMMENT ON TABLE col.sintoma_estado IS 'Guarda los estado por los que pasa un paciente luego de haber registrado sus sintomas';
          col          postgres    false    224            �            1259    250951    sintoma_estado_id_se_seq    SEQUENCE     �   CREATE SEQUENCE col.sintoma_estado_id_se_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ,   DROP SEQUENCE col.sintoma_estado_id_se_seq;
       col          postgres    false    224    7            �           0    0    sintoma_estado_id_se_seq    SEQUENCE OWNED BY     O   ALTER SEQUENCE col.sintoma_estado_id_se_seq OWNED BY col.sintoma_estado.id_se;
          col          postgres    false    223            �            1259    242814    sintoma_id_sin_seq    SEQUENCE     x   CREATE SEQUENCE col.sintoma_id_sin_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 &   DROP SEQUENCE col.sintoma_id_sin_seq;
       col          postgres    false    212    7            �           0    0    sintoma_id_sin_seq    SEQUENCE OWNED BY     C   ALTER SEQUENCE col.sintoma_id_sin_seq OWNED BY col.sintoma.id_sin;
          col          postgres    false    211            �            1259    242878    ztmp_persona    TABLE     <  CREATE TABLE col.ztmp_persona (
    id_pertmp bigint NOT NULL,
    fk_ciu integer,
    movil character varying(20) NOT NULL,
    nombres character varying(150) NOT NULL,
    apellidos character varying(150),
    alias character varying(40),
    telefono character varying(40),
    direccion character varying(150),
    direccion2 character varying(150),
    email character varying(120),
    fecha_nacimiento date,
    tipo_sangre character(1),
    rh character(1),
    sisben integer,
    eps character varying,
    localizacion jsonb,
    codigo character varying(5)
);
    DROP TABLE col.ztmp_persona;
       col         heap    postgres    false    7            �            1259    242876    ztmp_persona_id_pertmp_seq    SEQUENCE     �   CREATE SEQUENCE col.ztmp_persona_id_pertmp_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 .   DROP SEQUENCE col.ztmp_persona_id_pertmp_seq;
       col          postgres    false    7    218            �           0    0    ztmp_persona_id_pertmp_seq    SEQUENCE OWNED BY     S   ALTER SEQUENCE col.ztmp_persona_id_pertmp_seq OWNED BY col.ztmp_persona.id_pertmp;
          col          postgres    false    217            �
           2604    242793    departamento id_dpto    DEFAULT     v   ALTER TABLE ONLY col.departamento ALTER COLUMN id_dpto SET DEFAULT nextval('col.departamento_id_dpto_seq'::regclass);
 @   ALTER TABLE col.departamento ALTER COLUMN id_dpto DROP DEFAULT;
       col          postgres    false    209    210    210            �
           2604    242782    dolencia id_dol    DEFAULT     l   ALTER TABLE ONLY col.dolencia ALTER COLUMN id_dol SET DEFAULT nextval('col.dolencia_id_dol_seq'::regclass);
 ;   ALTER TABLE col.dolencia ALTER COLUMN id_dol DROP DEFAULT;
       col          postgres    false    208    207    208            �
           2604    242770    funcionario id_fun    DEFAULT     r   ALTER TABLE ONLY col.funcionario ALTER COLUMN id_fun SET DEFAULT nextval('col.funcionario_id_fun_seq'::regclass);
 >   ALTER TABLE col.funcionario ALTER COLUMN id_fun DROP DEFAULT;
       col          postgres    false    206    205    206            �
           2604    250972    mercado id_mer    DEFAULT     j   ALTER TABLE ONLY col.mercado ALTER COLUMN id_mer SET DEFAULT nextval('col.mercado_id_mer_seq'::regclass);
 :   ALTER TABLE col.mercado ALTER COLUMN id_mer DROP DEFAULT;
       col          postgres    false    225    226    226            �
           2604    242831    persona id_per    DEFAULT     j   ALTER TABLE ONLY col.persona ALTER COLUMN id_per SET DEFAULT nextval('col.persona_id_per_seq'::regclass);
 :   ALTER TABLE col.persona ALTER COLUMN id_per DROP DEFAULT;
       col          postgres    false    214    213    214            �
           2604    242906    persona_nucleo id_nuc    DEFAULT     p   ALTER TABLE ONLY col.persona_nucleo ALTER COLUMN id_nuc SET DEFAULT nextval('col.nucleo_id_nuc_seq'::regclass);
 A   ALTER TABLE col.persona_nucleo ALTER COLUMN id_nuc DROP DEFAULT;
       col          postgres    false    219    220    220            �
           2604    242842    persona_sintoma id_ps    DEFAULT     x   ALTER TABLE ONLY col.persona_sintoma ALTER COLUMN id_ps SET DEFAULT nextval('col.persona_sintoma_id_ps_seq'::regclass);
 A   ALTER TABLE col.persona_sintoma ALTER COLUMN id_ps DROP DEFAULT;
       col          postgres    false    216    215    216            �
           2604    250944    persona_sintoma_estado id_pse    DEFAULT     �   ALTER TABLE ONLY col.persona_sintoma_estado ALTER COLUMN id_pse SET DEFAULT nextval('col.persona_sintoma_estado_id_pse_seq'::regclass);
 I   ALTER TABLE col.persona_sintoma_estado ALTER COLUMN id_pse DROP DEFAULT;
       col          postgres    false    222    221    222            �
           2604    242753    plataforma id_pla    DEFAULT     p   ALTER TABLE ONLY col.plataforma ALTER COLUMN id_pla SET DEFAULT nextval('col.plataforma_id_pla_seq'::regclass);
 =   ALTER TABLE col.plataforma ALTER COLUMN id_pla DROP DEFAULT;
       col          postgres    false    204    203            �
           2604    242819    sintoma id_sin    DEFAULT     j   ALTER TABLE ONLY col.sintoma ALTER COLUMN id_sin SET DEFAULT nextval('col.sintoma_id_sin_seq'::regclass);
 :   ALTER TABLE col.sintoma ALTER COLUMN id_sin DROP DEFAULT;
       col          postgres    false    212    211    212            �
           2604    250956    sintoma_estado id_se    DEFAULT     v   ALTER TABLE ONLY col.sintoma_estado ALTER COLUMN id_se SET DEFAULT nextval('col.sintoma_estado_id_se_seq'::regclass);
 @   ALTER TABLE col.sintoma_estado ALTER COLUMN id_se DROP DEFAULT;
       col          postgres    false    223    224    224            �
           2604    242881    ztmp_persona id_pertmp    DEFAULT     z   ALTER TABLE ONLY col.ztmp_persona ALTER COLUMN id_pertmp SET DEFAULT nextval('col.ztmp_persona_id_pertmp_seq'::regclass);
 B   ALTER TABLE col.ztmp_persona ALTER COLUMN id_pertmp DROP DEFAULT;
       col          postgres    false    218    217    218            }          0    242790    departamento 
   TABLE DATA           S   COPY col.departamento (id_dpto, departamento, divipola, iso, ciudades) FROM stdin;
    col          postgres    false    210   �s       {          0    242779    dolencia 
   TABLE DATA           F   COPY col.dolencia (id_dol, dolencia, observacion, imagen) FROM stdin;
    col          postgres    false    208   i�       y          0    242767    funcionario 
   TABLE DATA           V   COPY col.funcionario (id_fun, fk_per, fk_pla, usuario, password, esquema) FROM stdin;
    col          postgres    false    206   ��       �          0    250969    mercado 
   TABLE DATA           M   COPY col.mercado (id_mer, fk_per, estado, observacion, creacion) FROM stdin;
    col          postgres    false    226   Ә       �          0    242828    persona 
   TABLE DATA           �   COPY col.persona (id_per, fk_ciu, movil, nombres, apellidos, alias, telefono, direccion, direccion2, email, fecha_nacimiento, tipo_sangre, rh, sisben, eps, localizacion, imei) FROM stdin;
    col          postgres    false    214   f�       �          0    242903    persona_nucleo 
   TABLE DATA           S   COPY col.persona_nucleo (id_nuc, fk_cabeza, fk_integrante, parentesco) FROM stdin;
    col          postgres    false    220   ��       �          0    242839    persona_sintoma 
   TABLE DATA           [   COPY col.persona_sintoma (id_ps, fk_per, fk_sin, valor, observacion, creacion) FROM stdin;
    col          postgres    false    216   �       �          0    250941    persona_sintoma_estado 
   TABLE DATA           c   COPY col.persona_sintoma_estado (id_pse, fk_ps, fk_fun, fk_est, observacion, creacion) FROM stdin;
    col          postgres    false    222   �       v          0    242748 
   plataforma 
   TABLE DATA           E   COPY col.plataforma (id_pla, fk_ciu, fk_dpt, plataforma) FROM stdin;
    col          postgres    false    203   !�                 0    242816    sintoma 
   TABLE DATA           R   COPY col.sintoma (id_sin, fk_dol, sintoma, requiere_obs, observacion) FROM stdin;
    col          postgres    false    212   l�       �          0    250953    sintoma_estado 
   TABLE DATA           4   COPY col.sintoma_estado (id_se, estado) FROM stdin;
    col          postgres    false    224   ɜ       �          0    242878    ztmp_persona 
   TABLE DATA           �   COPY col.ztmp_persona (id_pertmp, fk_ciu, movil, nombres, apellidos, alias, telefono, direccion, direccion2, email, fecha_nacimiento, tipo_sangre, rh, sisben, eps, localizacion, codigo) FROM stdin;
    col          postgres    false    218   B�       �           0    0    departamento_id_dpto_seq    SEQUENCE SET     D   SELECT pg_catalog.setval('col.departamento_id_dpto_seq', 33, true);
          col          postgres    false    209            �           0    0    dolencia_id_dol_seq    SEQUENCE SET     >   SELECT pg_catalog.setval('col.dolencia_id_dol_seq', 2, true);
          col          postgres    false    207            �           0    0    funcionario_id_fun_seq    SEQUENCE SET     B   SELECT pg_catalog.setval('col.funcionario_id_fun_seq', 1, false);
          col          postgres    false    205            �           0    0    mercado_id_mer_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('col.mercado_id_mer_seq', 8, true);
          col          postgres    false    225            �           0    0    nucleo_id_nuc_seq    SEQUENCE SET     <   SELECT pg_catalog.setval('col.nucleo_id_nuc_seq', 4, true);
          col          postgres    false    219            �           0    0    persona_id_per_seq    SEQUENCE SET     >   SELECT pg_catalog.setval('col.persona_id_per_seq', 10, true);
          col          postgres    false    213            �           0    0 !   persona_sintoma_estado_id_pse_seq    SEQUENCE SET     M   SELECT pg_catalog.setval('col.persona_sintoma_estado_id_pse_seq', 1, false);
          col          postgres    false    221            �           0    0    persona_sintoma_id_ps_seq    SEQUENCE SET     E   SELECT pg_catalog.setval('col.persona_sintoma_id_ps_seq', 1, false);
          col          postgres    false    215            �           0    0    plataforma_id_pla_seq    SEQUENCE SET     @   SELECT pg_catalog.setval('col.plataforma_id_pla_seq', 1, true);
          col          postgres    false    204            �           0    0    sintoma_estado_id_se_seq    SEQUENCE SET     C   SELECT pg_catalog.setval('col.sintoma_estado_id_se_seq', 9, true);
          col          postgres    false    223            �           0    0    sintoma_id_sin_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('col.sintoma_id_sin_seq', 7, true);
          col          postgres    false    211            �           0    0    ztmp_persona_id_pertmp_seq    SEQUENCE SET     F   SELECT pg_catalog.setval('col.ztmp_persona_id_pertmp_seq', 10, true);
          col          postgres    false    217            �
           2606    242795    departamento departamento_pkey 
   CONSTRAINT     ^   ALTER TABLE ONLY col.departamento
    ADD CONSTRAINT departamento_pkey PRIMARY KEY (id_dpto);
 E   ALTER TABLE ONLY col.departamento DROP CONSTRAINT departamento_pkey;
       col            postgres    false    210            �
           2606    242787    dolencia dolencia_pkey 
   CONSTRAINT     U   ALTER TABLE ONLY col.dolencia
    ADD CONSTRAINT dolencia_pkey PRIMARY KEY (id_dol);
 =   ALTER TABLE ONLY col.dolencia DROP CONSTRAINT dolencia_pkey;
       col            postgres    false    208            �
           2606    242776    funcionario funcionario_pkey 
   CONSTRAINT     [   ALTER TABLE ONLY col.funcionario
    ADD CONSTRAINT funcionario_pkey PRIMARY KEY (id_fun);
 C   ALTER TABLE ONLY col.funcionario DROP CONSTRAINT funcionario_pkey;
       col            postgres    false    206            �
           2606    250978    mercado mercado_pkey 
   CONSTRAINT     S   ALTER TABLE ONLY col.mercado
    ADD CONSTRAINT mercado_pkey PRIMARY KEY (id_mer);
 ;   ALTER TABLE ONLY col.mercado DROP CONSTRAINT mercado_pkey;
       col            postgres    false    226            �
           2606    242911    persona_nucleo nucleo_pkey 
   CONSTRAINT     Y   ALTER TABLE ONLY col.persona_nucleo
    ADD CONSTRAINT nucleo_pkey PRIMARY KEY (id_nuc);
 A   ALTER TABLE ONLY col.persona_nucleo DROP CONSTRAINT nucleo_pkey;
       col            postgres    false    220            �
           2606    250960    persona persona_imei_key 
   CONSTRAINT     P   ALTER TABLE ONLY col.persona
    ADD CONSTRAINT persona_imei_key UNIQUE (imei);
 ?   ALTER TABLE ONLY col.persona DROP CONSTRAINT persona_imei_key;
       col            postgres    false    214            �
           2606    242836    persona persona_pkey 
   CONSTRAINT     S   ALTER TABLE ONLY col.persona
    ADD CONSTRAINT persona_pkey PRIMARY KEY (id_per);
 ;   ALTER TABLE ONLY col.persona DROP CONSTRAINT persona_pkey;
       col            postgres    false    214            �
           2606    250950 2   persona_sintoma_estado persona_sintoma_estado_pkey 
   CONSTRAINT     q   ALTER TABLE ONLY col.persona_sintoma_estado
    ADD CONSTRAINT persona_sintoma_estado_pkey PRIMARY KEY (id_pse);
 Y   ALTER TABLE ONLY col.persona_sintoma_estado DROP CONSTRAINT persona_sintoma_estado_pkey;
       col            postgres    false    222            �
           2606    242847 $   persona_sintoma persona_sintoma_pkey 
   CONSTRAINT     b   ALTER TABLE ONLY col.persona_sintoma
    ADD CONSTRAINT persona_sintoma_pkey PRIMARY KEY (id_ps);
 K   ALTER TABLE ONLY col.persona_sintoma DROP CONSTRAINT persona_sintoma_pkey;
       col            postgres    false    216            �
           2606    242758    plataforma plataforma_pkey 
   CONSTRAINT     Y   ALTER TABLE ONLY col.plataforma
    ADD CONSTRAINT plataforma_pkey PRIMARY KEY (id_pla);
 A   ALTER TABLE ONLY col.plataforma DROP CONSTRAINT plataforma_pkey;
       col            postgres    false    203            �
           2606    250958 "   sintoma_estado sintoma_estado_pkey 
   CONSTRAINT     `   ALTER TABLE ONLY col.sintoma_estado
    ADD CONSTRAINT sintoma_estado_pkey PRIMARY KEY (id_se);
 I   ALTER TABLE ONLY col.sintoma_estado DROP CONSTRAINT sintoma_estado_pkey;
       col            postgres    false    224            �
           2606    242825    sintoma sintoma_pkey 
   CONSTRAINT     S   ALTER TABLE ONLY col.sintoma
    ADD CONSTRAINT sintoma_pkey PRIMARY KEY (id_sin);
 ;   ALTER TABLE ONLY col.sintoma DROP CONSTRAINT sintoma_pkey;
       col            postgres    false    212            �
           2606    242886    ztmp_persona ztmppersona_pkey 
   CONSTRAINT     _   ALTER TABLE ONLY col.ztmp_persona
    ADD CONSTRAINT ztmppersona_pkey PRIMARY KEY (id_pertmp);
 D   ALTER TABLE ONLY col.ztmp_persona DROP CONSTRAINT ztmppersona_pkey;
       col            postgres    false    218            }      x�m\ɲ�:�]�_�x�
q&{Q���A�|�}�'o�^Tx���F?�'� ��u8�@"�� �7n���޻CA���Gq:%���[w���}��GSݹ�����.��s~g0�����p�ey~wg'����U�D����7^�U���k7<���+^>�۾�r~�8-ڕ�wzŇ���������
y�C;��ݲ�����t9OC�i�
�rA��iE�Э�<t�{W�j�Q�n�����h�h6gOj2�v}_�3����]������-�T�������&	?a_��o僓ԏ�l�E>-I���7_�޴ah�8��'�u���{7��g�u���:-v5OE��R?,��	X	�͊�gvñ�CK�x�ϓ�aR���(�һ�u�W�}x�ѱI'7i��~�e��6܇���Q*=���m7����(p�0�Vޔ&��iv�;;�S�#v�"��
�]���6,~}ʒܝ��>��6�� ��u�+�1�K�@��iY2|�N�/�}��L�JTZ�MX#_��W^!��E�Z��2�,e�������=����2v]����,_w �>w�.� �7�ln~~c�e�+��ng��{�ζ8��w�ߠ]�#ҕ�	c;�	���CT�e���G��"�U~wl���Ou+{n�{ȯ|p~RtMN���f �U!�y��\�q�r����%��$/� �0K]�y��b1�*�9��o�����DU9wz� ޺��]��u�4�O������� H�ȟ;Oa�7��y��P�-X^��xfQ喺�Z�Iyk?�����7��d��t�N��{ɪn�~��X�~��biͶ��� ��_U��p'2\����MK�C���i�V�A-�GfO�j�h�A;W�	:�aM@�Ƞ�E��=HIӆ����D�����\c���t�%��g����X̒%����,��o� u��-�_.zy�����\�RPV��eRI(+{�F�{|üw2��vvO_`���2��[��0�ڿF_��L�^�u��ʑڠSz�+�kw��)*�i\uiV�(�i���U�����֭2��Co�/TM*�ʯ��l0�K��T'�>�R�Y񃼜`��p�z�Ú�nߒkبݡ�{����(�{~�n�ݻ��%]�w�;,s���K�b���o��xk^��N�D]W,�Mlo�C�9��,j�f��9�b�Y�<����p��%;���������3��z�A{t��(s7�U�kraL�o�
3֦�Lc�:؅n�l�]���%uzb-*�X��t![pi��?����D��,Q�>��:��Ē���)��h;՚	f��*���,$_P�!���@\a/4q�(ϭxZYOk�i,D}��ɘ��ӡ��7���d�"�ʶ��c����)���C�
��tk1bVfʗ�?�0�
2]{��/ze�~��>�˹K�H�_f����ف2+�M���S�e	_x;��+3�5��}���<�	��`ԉ5�}��B!<p4���;��$+��y��>0�j���FnRu��cˣ+���^���H}����ӡ��8S�u�E���A���Ew��a�靠����?HJy��A�`�>�U�<��;)dގ���v^솄��2���ŉQ�F�_������dE����˵7z�*��+��]@�~0ZT�(AelQ7YI��3�:r4�l��]-�������^��L�M��)�!�^"��OU-8��L|&~�m��#����6�j-y�X|��u�0��zd�
F��[9\3Y��F���ew��u�e<z�������F�K��������+
�Hs?}�8��)x�Ӹww~j��h{�t��ʆJVE�͍|V�.�E
�R)�<�=rF.��c�uYa��������p��������F��]��䥔11�1g���?�ڃ��x���9��~H2�MfPull��=�@�kh簈Zv ]�i�F��PXZ�[`"F�H@�@+@�@��9�C�����X&[a�g�)�M�ߘ,+�hB��~b�rh�ow1q��� S�|#Yʑ�� @�+4L y�7�
��"�d�w��Fv����L��~�Ξ(�"��G�E�	5�@SA�SaY�݇6��x��B�n�˼�6?��Y�o���6azu�mQ��8Ʌ{�Kw�/��O��L��b�!�.S��K�g�����N�*�.�љ���1����fx�.dE,']ao�
i��鶃��������_�-rDfwV�++%IXڭۜ���TGM����_a��u�a�X�<]��k���>���a\�<��K�����C˯�Pk֧��ְ�0�ET�A���l�P�d(�`��ʧk�g����x�}�,N�蠵;!P�x���1ʼC0�Mrqb�&] �U���N�b�{����Od��஋�H�ї�+Ɓ�(�I`�+�����z��.�L�F1	*̌�w�mZ����n�Ǉ>�"�c�W{ %Z�����-kk/d�f������ą&����9��c���< ����Qԧ�${݄�{| _T
@��`Ӌ3J<�n�E�K6�&叇�f4�d���1Cu��v���Bv�˂����SA���:K���M�j��Eލ4�H�+%�r w�(aE��[O���%U�,��~:�
����-;��,Ր�g��,��?|���}�,�
9��5�ܗUݷ�,�U^Z����$Q�u��ɤ>\�mZzÕO�A�s��ym��}�L��SnQ�Ϣ!���?e<xZfQ�Z@�Avw)n����+���-Z�}Ȁ��P��y�KXMzmL\RiAQ����^ش��x�j��L��\�Ι�_��"u�l�0�% c�,C3Q�N�����hs`�Q	�xЬhT8O�ܭ�^��z�W{|G^�����̶*^�Z�?mf�T|���kKv"�X&.^�/R6��
��]-sqro�GQD��씆P<����EN�x%�A��_�D����jq�l6E�����0��9Ξd0]��^�:oQ���j�O�3[['AA�"���ӛ�Z)k#�k³�+g)*&��x�O��4~4a���M�a�`��r	?OW�EQo�:���O*U)/�\��+�I\g�^Iy_��U2?;��5�ڰuf�-d�Z��Sᚚ�����TlUf�o�A���F���֬�IźfU�M��Ɠ�=;��5�mR�b�专FԷ2m+��숓����F#Ai~����i��+�9`y��n��3��=:�E�%Ɂy�!���Jk	@�9�'	�O	*&>긂�9�Xk9o���=r&�!�_���7���*�"�ƿ����Y��_���-mwa�\?�rT��`�>&�r�(��4�\%�N�LEYY�?"y�)DG����]iuUS��uS���ݸ���8;�&���i�[q6 r�Dc{�jP�=��ULiz�(Y�u/�3�u�`Rե���gT��D�r1+���d�a޷�k:�%��8�oP4�II�GA ^_�Zں,�<��sp�Wm�s�Q���8 {�7�9�G����Cb�l����S���$�%����l�o��U��y��n8�O!��iZ١�������w�|�%9\ZZ����/�j�E�����~Х��{'�U�Mp����hl�Ze����I�0ux	�4	kSr�(���jk,��AY�IA���iTL+M�IVec��b�&ds6gdk�j$XD���EқP��$��xn{s8+8��˅�7�����p��]�&�wP�S�9�4�S�x�IQA��8ED[�L�PR�J�]�) V�ئȄ�n�Be���a,5��᥷��?�0Xភu�u�q�  �/#[�P!���*���&�e�5pz|ș�=؊�jeɎ8��T\�@�Rg��8��i��
˻�hmo�j65;	[�i�`�Y#�M�ܘTati*l�JI��EL	oc����}`zy���X�y.�
��	M�!ʛ�D)_�,�h*��5�y��
��\�(V΍�&FS&
ox+���e�0����L��~���)k 4k��,wHB��;�8    �i@J�b8�p��)�;����5��}�5��ICG� �t�'�=4�7���!&|�Y�g,C'��rSY
k��0��4ymA���#�C���'�oP[!�RCEP5{���&P�����J�y��1�g�;���+���W��W;�,jk�T���7w��L��n�,�AV~�T���(�zR�<���[ӓ�՛��>�$f7�y�+"��Wn��{<zid��JK��t�Z(�~��ZaYzJ*��hd$$Ax-�Ba\�@)��$R@��Y��4w�Z����c�d�Jh�Wi]�D6d`�Բ��A9Tz
iN]HGH��*ў+o�es<_���JIO��]O� ��/����%#j�h�I�%��ZgQ<����;�P� �D"&xX̗��*���%�@�Gu�X��4�!���$Y��D�*e�@���f�Y���đ7��rT��`S$[1�����E��+·�f�xiN�%# �e�N�T$��0�5�G�UZ4�v��B�m�V�\L3	��&*c�����<��cx�H����X�u��.�E�S�8�$N�T�*5���ZFSۯe�if!!����
����Ȝ�5)C��"H��F�ϔ�����9t���?���a(����ZDɯ'y�Q����[��_O�s�*1\w*E�*��OcC1|A��kjG�VI�!�**�N�� �n�zv�Ө�~pd��-��v�F��V7��ڹ��p;ϓ�MR+<w�E	a��w��O��}���\��gR�B�sn��h���P
�ic��gm�e�b��!P�Q0k� ���R�i���57�p��[�J���l%ГFٕ}��v#[/oM�`����M^p�fƛa��9JZ��P6��oJ���&��ٔ}|R��Qo�i_����jo��.C !�s�+Ż0v��Qq�x�hE����E��i����'a"S���(�cߝ�|��t��j�Pu?���k���.g!#D�:���vx4ߖEV���D����Z�Ψ�!d~ Ճ����
�[�*�{#w]Ax�	���?UO4�4>��@�J`+`��0e��RH�,g�D�ܣB�R�~˯�I��6�&�9K��vU��-(2K�+8���S��X���7�v��A�I�����0g��5�J�����"'�ZG����F�|��`%c=�*�X��ң�W��]� s3�T�4�l}b��z��Y��ZJi�����K�`v�%腖2�N�B�(e��U"w�a���{%G_�l�q��0s��&��$�HA)[��}&H��fvɁ�"]�s�G,�b�lzC�٪���}�YI.�2.B������5h|�ɭh���j�l��$�	�A^��Ce�Yok���6QBS�SR�9h���Ѐl��<�{�vT��R4e��լ�lҟk�' ���W�&q�`�k��r��SR<te�X�*����Kg��v��O6�V����z�AI��<����>����<Ҋ~g�,	.:K�<aR��[�T:��u{�V�T�:	TI`���e�%Zs%���8���&I۴��_�~#�K�.��{�\����ŐUZ!��uם�g����?�����;�_��֛��L+��n�� ��t	㓳·c�$�����j�|C<d����~��y�0�`	��F��{�O�֓�z�
��a��ev �ty�9��|��@��|j���_蓎+�u�6]�Q
E���������'�⒚_;)"D亻K��Ft �Ld R�p��H/�Zʊp��`(��(��$��O��yb�:C�� �L@��5��1�*yu�4J--N�S�&���Y�.�T�g��댓��:N�R	���\�Q�|hh�����<Q����I29�*��4ce̊�iy_�D���&]4*%�����`s�h1D�2OL*5FHer����j�q�IC�>�a��R�"��6J��
��\5K�XI�]�I-�� �ݠ���5����7T:+�3K@�X�zO���]/�cT����� (��O��n�Je�"�y��ck�Z�#Nb��MX�QlsR�*�M.���>�����7�d�#|�9�"�7��[�����-!�P����%�+�0���ȑ�Cb���cR��c ��d#�Y�����F�DR���UE�P�,�]��z�v�:�ϯt<�\�A��=���+_8�l�!�Ɵ�h#U�� <N�j@�&J_ �I�<����20U(���~c%U5�q� �=����G���#M7P�Cq���ЈS-��"U���Lk;�O��<�g����H(ĝ�O'xO�)�ͳȁ�喸^�Χ��ԛX�(��VQp��p��+e�RDQ��o;m��t�v�7��O�H: -���!�szʔU�ݦ#B�Td�����o״| �FZ�e��s
N��W�o֗�j|
Z[����z$F��6��l��ef���S}�4���[�Ci/�Z˸Ul��FY��T%"�Xn\��%9;Hd�P����9�,��@~����>\�;�.�r�ñ p#�؇S-G|hȧHC=���$���D�t!H�W��L$�HGH��PV�ў�{t�����`%��,r�E��s�9r
\��!�9��r|��Z��%��z�h��jR��m�7G��'�@�8 ���D�4��~N���؉�3�:�Cg<�MS�`O�O�o��$�G?�.\fDNw�Q�fǨڤW��,�Kig�4�M�If3��L`*�ʸ���V�F��7M�~��0�DS�B-�~��Q�>�褉T�+�C$���  �Zbn�+ēa"��"i�eS� c���*��(���G{X
����O���Ml��4���1I4iV��%{	Դ�e�3d�5�Ze*>z(*_̽&���Z�[�.����	h
�/q461�:k��S=j(��%�E*Ũ?�ݨhO׼nb��0�!������4r~���B����P ]���B���w+u���az~=/:�O�wTC����n�K�E�Qt�D� �AHTy�V+���7��Ơ�k�&u�c��p�Н���1��
���^��v����pю�������r�*�NPv{߉e�0v �bc�a���:����u�|��4��%������ݦ�䦕�EuRδ�N��~~��ډ(����y��|ی�A=W#/��ژ�I�nt�b���Y�J�1�1���!y�� ����(��r�-��2!����ё�����8���3W ���p�HBIb�O/{��*{��0��~ߩ8��Nq%���K�B�7��-�-��FS�і ��<��o롩�'�ת��N�W6���e_�r�@6��D�-NVj��3�HUS��L?킣X4�R��)��J���E߫
h|v���E�i��
���Vr�7 ��_�8!R)Ңh� �<V�h�ݕ���:���Pc0����;0��������(�3�w����Z'v�u���X9aZ)�5؄ɾF������۶U���zK� �E��o��	�8.!\�k{�d�!��P�%�<��,�M|<�r����f�Ʃ�i�]aJ[���BiXH��ד�����JX��R�U����RYm[$�����h���b�x>��60K�[�s����%T��dsR�w�����YW��&"ː�Ͽ�(:�V��r�cB���֤2���>0�[%���u�6{��K��-�����hG�b�y�ݞ_�*�v�lj/�i��u�T>�x �5z�YA����-�r� ����*��5#@Pn� A{�y-�����X7�}w��]7ږᔏ�}��[��c�F��I���?��1�E\T��p�����o�x�M�C)T�)�\����#o�c}�F�Rጲ㵗���y���Q~݇U��?�*T�^Np�IoT��l��ʣ)�w�qEk�YHv;�� �.?��T7���]+s���KX�F8F�-��J��!4�V��7)o%�����-�X���z�J�0S�Չ~�BW�\{:�KlVu�v���kB�d?�R�O?am q  s:�m��,��ĐNw*�Y�4�ʔ�L�t���h�J-2�N�;OhP?�j�\�6��fi竂��a���(i���!�i�Z��H�m�(����UsĢ�pcu���#"%�n7S����Jl!N���t
�g�}]A�h_��K��BF���Et���z�v�;R�T6 ��Q [� VΡ��	�C���0�X��0�����$qJ���C!5�>2ׇ��_-!\��I�/b)�� p1��Z iI�^��-4+����T����*Yv2�]Լ �E�Z4L���O�8z�l�e'��ȏ��e�^èH�\��S�8��XG�Gȷ�1���$�W]Q����&	�TY~��V�4Lbg�FX���H^�;�/ꓮ�u�im �}�p� 0�o���f�*�!W��d-��s?�HEvX�H\��[l�9Hc�}տA<��RɄl�r�@ug4�����`��*��,A����^�E�Q�������*{9���v".f�lOV���1#���8���~������x+�)i%B���N��ݲ��#+�V��)�Ȯ��ni�_Xu����!c�9�)[�-OG9H�+a#eU�f�$����9���Wҙ��Wk�#��P�pl�.�;�۵{PI-mcf|��8�c�5*Y�b/��H�����J�#
k2 X��pxp'��t��)���B�Ui�#�Q
y/¸ ڦ�]k� �B[:�P�鷦���ě���c1��������ӕSF�S�74��H���ڪ-k��R&�e|:����J)�q�J��zQ��m�?��j>af}W���=\���/�w��P9S��=Ev��#��KR�=D��7�et���w��~��pR}N3[ ��H��aZ�o���z�=y�ǈ�!ӂ���#T�֨gJ��w-����-��
GF�e����#j=�]�s��sd�$21���Z��t-���D?�n�'��9+9�����@Q��(���bN ;=�Qκ�S�'�j��F���q�z�����,;��TL���~SxO�Ćn�%Y�$�t��Hu�3L4p�U�>Y:�ps�!��ty�����#8��ө�>��>�!T.������M�B5���(s͂5I�r��K��I��Q�V니��__�|� �      {   =   x�3�t���s�
��<]-9���2S�
�ҹ�8]\��C]aTJj^zi*X*F��� <cs      y      x������ � �      �   �   x��̱1�ڙ�β�c��+hN��?
��?=���q^o��8�y���&�L/�^vb�ě����<JP*l�>��T~y���a�����Xa������5|�\ph�pdF�|�Xh�%��?���\-M      �   '  x���Mk�  ��_�}��$&�ַ�&e0B/�c#_l�4��g�QZX�v�(*>�*�(�<
}X�9�H����K)�T�d��˩��I@$�<E��f�jڶ�SW?�����	s�Х>(��Qf��	�8�}�]d���X�Z��Т"[��@��44u��R���0��8v����Yw9���	�	=��,�9s��ab�%�3��B�r������t����^�'�>���ؠֹ�.�@���>�Du�z-�¯���?eHw&K�.������HN?�ݯ�����      �   :   x�3�4�4�t��p<<�5�_�Q��(f�������e�i	�P�	��A�1z\\\ �Q:      �      x������ � �      �      x������ � �      v   ;   x�3�43300�4��qqt��uTpqU
uurVp�sq��tT0202������ )�8         M  x���MN�0�דSx	$���m܎�4.v2b�1�+v\��N;���l���{�s�����k�`uP��)P�B0�D�L��*I��6oc/�Y��D�9�s%��B�L��*��_Ĭd�N��	F��_�%1�����;��Ir���bjy�����Mj�9'�psH3�Dn�E�~��X3�tF�Þ�ٛRp�O�R(a�]l�a��N�^iUd4Lp1I/vy������_y��k�C�Q�tfE=������u�:7s)/�6آ�Hc[
�ND_[u�q2����{��Uha�$RI��di[���$�&�����Ӧ���*���c�4      �   i   x�M�1�@��>Ş 	�/��,�8�:)�?GX�t3�L��|!�9!�g�͚��Jo�l�O�!u���;��F
tzPU��e�(�'�yd�@��=1��oH      �   �   x��ν�0F���Ut7���b7+4���FX�1��{׻��3>G�Z!$�4J��CQ�c�
'_M�%4��yl���;~�o=׼�,|U�L�|}�齾��m�s:��˲D�D��A�C[|��k��}M�o�}C�[b?#�wľ�Z�ֱ>e�� !��     